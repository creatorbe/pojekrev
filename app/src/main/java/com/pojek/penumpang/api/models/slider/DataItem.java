package com.pojek.penumpang.api.models.slider;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("foto")
	private String foto;

	@SerializedName("fitur_promosi")
	private String fiturPromosi;

	@SerializedName("id")
	private String id;

	public void setFoto(String foto){
		this.foto = foto;
	}

	public String getFoto(){
		return foto;
	}

	public void setFiturPromosi(String fiturPromosi){
		this.fiturPromosi = fiturPromosi;
	}

	public String getFiturPromosi(){
		return fiturPromosi;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"foto = '" + foto + '\'' + 
			",fitur_promosi = '" + fiturPromosi + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}