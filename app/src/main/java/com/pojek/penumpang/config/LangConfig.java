package com.pojek.penumpang.config;

/**
 * Created by AndrogoDS on 06/06/19.
 */


public class LangConfig {

    public static final boolean LANGUAGE_EN = true;
    public static final boolean LANGUAGE_HI= true;
    public static final boolean LANGUAGE_AR = true;
    public static final boolean LANGUAGE_ES = true;
    public static final boolean LANGUAGE_FR = true;
    public static final boolean LANGUAGE_PT = true;
    public static final boolean LANGUAGE_DE = true;
    public static final boolean LANGUAGE_IN = true;
}
