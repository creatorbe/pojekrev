package com.pojek.penumpang.home.submenu.home.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pojek.penumpang.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerPromoChildAdapter extends RecyclerView.Adapter<RecyclerPromoChildAdapter.ViewHolderChildPromo> {

    private List<DummyChildPromo> items = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolderChildPromo onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolderChildPromo(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_promo_slide_child, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderChildPromo viewHolderChildPromo, int i) {
        viewHolderChildPromo.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setRecyclerData(List<DummyChildPromo> data){
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    // viewholder child promo
    public class ViewHolderChildPromo extends RecyclerView.ViewHolder {
        private ImageView ivPromoChild;
        private TextView tvTitlePromo;

        public ViewHolderChildPromo(@NonNull View itemView) {
            super(itemView);
            ivPromoChild = itemView.findViewById(R.id.iv_promo_child);
            tvTitlePromo = itemView.findViewById(R.id.tv_title_promo_child);
        }

        public void bind(DummyChildPromo data){
            Glide.with(itemView.getContext()).load(data.getImage()).centerCrop().into(ivPromoChild);
            tvTitlePromo.setText(data.getTitle());
        }
    }
}
